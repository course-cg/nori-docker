FROM ubuntu:22.04

RUN apt-get update && \
    apt-get install -yq \
        git cmake build-essential ccache \
        zlib1g-dev libx11-dev libxrandr-dev libxinerama-dev libxcursor-dev libxi-dev libglu1-mesa-dev libxxf86vm-dev

RUN mkdir /nori_libs/
COPY ./nori /nori_libs/nori
RUN mkdir /nori_libs/nori_build
WORKDIR /nori_libs/nori_build

RUN cmake /nori_libs/nori -DNORI_HEADLESS=ON
RUN make -j

COPY ./PrecompiledLibs.cmake /nori_libs/.
COPY ./test_scene /nori_libs/test_scene

ENV NORI_EXT_DIR=/nori_libs/nori/ext
ENV NORI_EXT_BUILD=/nori_libs/nori_build/ext_build
