# Before running
docker login registry.ethz.ch # -u $USERNAME

if ! [ -d nori ]; then
    git clone --recurse-submodules git@gitlab.inf.ethz.ch:OU-CG-TEACHING/nori-base-24.git nori
fi

docker build . -t registry.ethz.ch/course-cg/nori-docker
# docker tag 22.04 registry.ethz.ch/course-cg/nori-docker
docker push registry.ethz.ch/course-cg/nori-docker
