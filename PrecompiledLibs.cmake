
### HACKY WAY OF ALLOWING TO CACHE THE BUILD DEPENDENCIES IN THE DOCKER CONTAIENR ###

if (NOT DEFINED NORI_EXT_DIR OR NOT DEFINED NORI_EXT_BUILD)
  if (DEFINED ENV{NORI_EXT_DIR} AND DEFINED ENV{NORI_EXT_BUILD})
    set(NORI_EXT_DIR $ENV{NORI_EXT_DIR})
    set(NORI_EXT_BUILD $ENV{NORI_EXT_BUILD})
  else()
    message(FATAL_ERROR "Need to define NORI_EXT_DIR and NORI_EXT_BUILD")
  endif()
endif()

set(OPENEXR_INCLUDE_DIRS
  ${NORI_EXT_DIR}/openexr/IlmBase/Imath
  ${NORI_EXT_DIR}/openexr/IlmBase/Iex
  ${NORI_EXT_DIR}/openexr/IlmBase/Half
  ${NORI_EXT_DIR}/openexr/OpenEXR/IlmImf
  ${NORI_EXT_BUILD}/openexr/OpenEXR/IlmImf
  ${NORI_EXT_BUILD}/openexr/OpenEXR/config
  ${NORI_EXT_BUILD}/openexr/IlmBase/config)
set(PCG32_INCLUDE_DIR
  ${NORI_EXT_DIR}/pcg32)
set(TFM_INCLUDE_DIR
  ${NORI_EXT_DIR}/tinyformat)
set(HYPOTHESIS_INCLUDE_DIR
  ${NORI_EXT_DIR}/hypothesis)
set(GLFW_INCLUDE_DIR
  ${NORI_EXT_DIR}/nanogui/ext/glfw/include)
set(GLEW_INCLUDE_DIR
  ${NORI_EXT_DIR}/nanogui/ext/glew/include)
set(NANOVG_INCLUDE_DIR
  ${NORI_EXT_DIR}/nanogui/ext/nanovg/src)
set(STB_IMAGE_WRITE_INCLUDE_DIR
  ${NORI_EXT_DIR}/nanogui/ext/nanovg/example)
set(NANOGUI_INCLUDE_DIR
  ${NORI_EXT_DIR}/nanogui/include)
set(EIGEN_INCLUDE_DIR
  ${NORI_EXT_DIR}/eigen)
set(TBB_INCLUDE_DIR
  ${NORI_EXT_DIR}/tbb/include)
set(FILESYSTEM_INCLUDE_DIR
  ${NORI_EXT_DIR}/filesystem)
set(PUGIXML_INCLUDE_DIR
  ${NORI_EXT_DIR}/pugixml/src)
set(INDICATORS_INCLUDE_DIR
  ${NORI_EXT_DIR}/indicators/include)


add_compile_definitions(NANOGUI_USE_OPENGL)
  
set(ExternalIncludeList
  OPENEXR_INCLUDE_DIRS PCG32_INCLUDE_DIR TFM_INCLUDE_DIR
  HYPOTHESIS_INCLUDE_DIR GLFW_INCLUDE_DIR GLEW_INCLUDE_DIR
  NANOVG_INCLUDE_DIR NANOGUI_EXTRA_INCS NANOGUI_EXTRA_DEFS
    NANOGUI_EXTRA_LIBS NANOGUI_INCLUDE_DIR EIGEN_INCLUDE_DIR
  STB_IMAGE_WRITE_INCLUDE_DIR TBB_INCLUDE_DIR
  FILESYSTEM_INCLUDE_DIR PUGIXML_INCLUDE_DIR
  INDICATORS_INCLUDE_DIR
)
foreach(Item ${ExternalIncludeList})
set(EXTERNAL_INCLUDE_DIRS "${${Item}}" ${EXTERNAL_INCLUDE_DIRS})
endforeach()

set(extra_libs GL Xxf86vm Xrandr Xinerama Xcursor Xi X11 pthread z dl)

# Compile in C++11 mode
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

set(ExtraLibList nanogui IlmImf IlmThread Iex IexMath Imath Half pugixml tbb)

file(GLOB_RECURSE LIB_FILES ${NORI_EXT_BUILD}/**)

set(LIB_FILES
    ${NORI_EXT_BUILD}
    ${NORI_EXT_BUILD}/tbb
    ${NORI_EXT_BUILD}/nanogui
    ${NORI_EXT_BUILD}/openexr/OpenEXR/IlmImf
    ${NORI_EXT_BUILD}/openexr/IlmBase/IlmThread
    ${NORI_EXT_BUILD}/openexr/IlmBase/Iex
    ${NORI_EXT_BUILD}/openexr/IlmBase/IexMath
    ${NORI_EXT_BUILD}/openexr/IlmBase/Imath
    ${NORI_EXT_BUILD}/openexr/IlmBase/Half
)

foreach(Lib ${ExtraLibList})
find_library(${Lib} NAMES ${Lib} "${Lib}-2_5" "${Lib}_static" PATHS ${LIB_FILES} NO_DEFAULT_PATH)
if(NOT ${Lib})
  message(FATAL_ERROR "${Lib} not found")
endif()

set(EXTRA_LIBS ${EXTRA_LIBS} "${${Lib}}" )
endforeach()

set(EXTERNAL_LIBS ${EXTRA_LIBS} ${extra_libs})
